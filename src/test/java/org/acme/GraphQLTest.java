package org.acme;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutionException;

import io.quarkus.test.junit.QuarkusTest;
import io.smallrye.graphql.client.GraphQLClient;
import io.smallrye.graphql.client.dynamic.api.DynamicGraphQLClient;
import jakarta.inject.Inject;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.JsonbConfig;

@QuarkusTest
class GraphQLTest {
    private static final String QUERY = "query{fetchData(fields:[{name: \"payload.product.suffix\", value: \"SE\"},{name: \"payload.product.productCode\", value: \"86\"},{name: \"payload.product.prefix\", value: \"UA\"}])}";
    private static final Jsonb JSON_B = JsonbBuilder.create(new JsonbConfig()
            .withNullValues(false)
            .withEncoding(StandardCharsets.UTF_8.name()));
    @Inject
    @GraphQLClient("dynamic-client")
    DynamicGraphQLClient client;

    @Test
    void testDynamicClient() throws ExecutionException, InterruptedException {
        io.smallrye.graphql.client.Response response = client.executeSync(QUERY);

        Assertions.assertEquals("{\"fetchData\":\"[{info={title=Product Reference Register, description=Product Reference Register, version=1.0.0}, payload={product={productCode=86, item_id_count=9D, productName=, productDescription=, productClass=Varubrev family, productCategory=Varubrev 1st Class, productStatus=Enabled, prefix=UA, suffix=SE, minimumWeight=0, maximumWeight=0, maxInsuranceAmount=0, activationDate=2022-01-01T00:00:00.000+01:00, deactivationDate=, dimension={length=0, width=0, thickness=0, circum=0, weight=0}}, country={countryId=, countryName=, countryAbbrevation=}, additional_service_codes=[48, 60]}, document_created_date=, document_updated_date=, id=60825044-bc4e-4372-8670-ec51aacd889e, _rid=CPAcAOyBpusDAAAAAAAAAA==, _self=dbs/CPAcAA==/colls/CPAcAOyBpus=/docs/CPAcAOyBpusDAAAAAAAAAA==/, _etag=\\\"04003430-0000-0d00-0000-652bafbf0000\\\", _attachments=attachments/, _ts=1697361855}]\"}", JSON_B.toJson(response.getData()));
    }
}
