package org.acme.configuration;

import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix = "app.azure.cosmos")
public interface CosmosConfig {

    String uri();

    String key();

    String database();

    String queryMetricsEnabled();

    String secondaryKey();

    String containerName();
}
