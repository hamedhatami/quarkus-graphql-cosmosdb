package org.acme.camel;

import com.azure.cosmos.CosmosException;

import org.acme.configuration.CosmosConfig;
import org.acme.model.exception.APIException;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.azure.cosmosdb.CosmosDbConstants;

import java.net.SocketTimeoutException;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import static jakarta.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import static jakarta.ws.rs.core.Response.Status.NOT_FOUND;
import static jakarta.ws.rs.core.Response.Status.SERVICE_UNAVAILABLE;
import static org.acme.util.Constants.AN_EMPTY_ARRAY;
import static org.acme.util.Constants.TIMER_RUN_ONCE_REPEAT_COUNT_1_DELAY_1;

@ApplicationScoped
public class CosmosRepository extends RouteBuilder {
    @Inject
    CosmosConfig cosmosConfig;

    AtomicReference<String> queryString = new AtomicReference<>();
    AtomicReference<String> response = new AtomicReference<>(new APIException(SERVICE_UNAVAILABLE.getReasonPhrase(), SERVICE_UNAVAILABLE.getStatusCode(), "Not yet ready to serve").toString());

    @Override
    public void configure() throws Exception {

        onException(
                SocketTimeoutException.class,
                RuntimeException.class,
                CosmosException.class,
                NullPointerException.class)
                .maximumRedeliveries(2)
                .useExponentialBackOff()
                .backOffMultiplier(2)
                .process(exchange -> {
                    response.getAndSet(new APIException(INTERNAL_SERVER_ERROR.getReasonPhrase(), INTERNAL_SERVER_ERROR.getStatusCode(), "CosmosException").toString());
                })
                .stop();

        from(TIMER_RUN_ONCE_REPEAT_COUNT_1_DELAY_1)
                .process(exchange -> {
                    exchange.getIn().setHeader(CosmosDbConstants.DATABASE_NAME, cosmosConfig.database());
                    exchange.getIn().setHeader(CosmosDbConstants.CONTAINER_NAME, cosmosConfig.containerName());
                    exchange.getIn().setHeader(CosmosDbConstants.QUERY, queryString);
                })
                .to("azure-cosmosdb://?accountKey=" + cosmosConfig.key() + "&databaseEndpoint=RAW(" + cosmosConfig.uri() + ")&operation=queryItems")
                .process(exchange -> {
                    final var value = exchange.getIn().getBody();
                    if (Objects.nonNull(value) && !value.toString().equals(AN_EMPTY_ARRAY)) {
                        response.getAndSet(exchange.getIn().getBody().toString());
                    } else {
                        response.getAndSet(new APIException(NOT_FOUND.getReasonPhrase(), NOT_FOUND.getStatusCode(), "The item can't be found").toString());
                    }
                })
                .end();
    }

    public String executeQuery(final String query) throws Exception {
        queryString.getAndSet(query);
        getContext().getRouteController().supervising().stopAllRoutes();
        getContext().getRouteController().supervising().startAllRoutes();
        return response.get();
    }
}
