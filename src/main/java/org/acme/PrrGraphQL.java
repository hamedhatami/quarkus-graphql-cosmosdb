package org.acme;

import org.acme.camel.CosmosRepository;
import org.acme.model.QueryFields;
import org.acme.model.exception.APIException;
import org.acme.util.QueryBuilder;
import org.eclipse.microprofile.graphql.Description;
import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Query;
import org.eclipse.microprofile.graphql.Source;

import java.util.List;

import jakarta.inject.Inject;

import static jakarta.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

@GraphQLApi
public class PrrGraphQL {
    @Inject
    CosmosRepository cosmosRepository;

    @Query
    @Description("Read data from cosmos database")
    public String fetchData(@Source List<QueryFields> fields) {

        final var query = QueryBuilder.build(fields);

        try {
            return cosmosRepository.executeQuery(query);
        } catch (Exception e) {
            return new APIException(INTERNAL_SERVER_ERROR.getReasonPhrase(), INTERNAL_SERVER_ERROR.getStatusCode(), e.getMessage())
                    .toString();
        }

    }
}