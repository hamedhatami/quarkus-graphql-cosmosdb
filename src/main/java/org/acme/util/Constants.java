package org.acme.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {
    public static final String DATE_FULL_MASK = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    public static final String DB_OPERATOR_LESS_THAN = "<";
    public static final String DB_OPERATOR_MATCH = "match";
    public static final String DB_OPERATOR_LESS_EQUAL = "<=";
    public static final String DB_OPERATOR_GREATER_THAN = ">";
    public static final String DB_OPERATOR_GREATER_EQUAL = ">=";
    public static final String DB_OPERATOR_FIELD = "field";
    public static final String DB_OPERATOR_LIMIT = " limit ";
    public static final String DB_OPERATOR_OFFSET = " offset ";
    public static final String DB_OPERATOR_AND = " AND ";
    public static final String DB_OPERATOR_FIELDS = "fields";
    public static final String FILTER_OPERATOR_LESS_THAN = "lt";
    public static final String FILTER_OPERATOR_LESS_EQUAL = "le";
    public static final String FILTER_OPERATOR_GREATER_THAN = "gt";
    public static final String FILTER_OPERATOR_GREATER_EQUAL = "ge";
    public static final String FILTER_OPERATOR_EQUAL = "=";
    public static final String DB_INDEX = "index";
    public static final String DB_SELECT = "select ";
    public static final String DB_FROM = " from ";

    public static final String DB_QUERY_WITH_FILTERS =
            " use keys ( select raw meta().id from %s let smeta = search_meta() "
                    + "where search(%s,  %s, %s)) where 1=1";
    public static final String DB_OPERATOR_QUERY = "query";
    public static final String TIMER_RUN_ONCE_REPEAT_COUNT_1_DELAY_1 = "timer://runOnce?repeatCount=1&delay=-1";
    public static final String AN_EMPTY_ARRAY = "[]";

}
