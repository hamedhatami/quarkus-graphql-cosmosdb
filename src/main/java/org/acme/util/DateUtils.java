package org.acme.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.experimental.UtilityClass;

@UtilityClass
public class DateUtils {
    public static boolean isDateBeforeToday(final String dateStr) throws ParseException {
        if (!dateStr.isBlank()) {
            Date today = new Date();
            Date date = new SimpleDateFormat(Constants.DATE_FULL_MASK).parse(dateStr);
            return today.after(date);
        }
        return false;
    }
}
