package org.acme.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class DbOperatorResolver {
    public static String getQueryOperator(String operator) {
        if (operator != null) {
            switch (operator) {
                case Constants.FILTER_OPERATOR_LESS_THAN:
                    return Constants.DB_OPERATOR_LESS_THAN;
                case Constants.FILTER_OPERATOR_LESS_EQUAL:
                    return Constants.DB_OPERATOR_LESS_EQUAL;
                case Constants.FILTER_OPERATOR_GREATER_THAN:
                    return Constants.DB_OPERATOR_GREATER_THAN;
                case Constants.FILTER_OPERATOR_GREATER_EQUAL:
                    return Constants.DB_OPERATOR_GREATER_EQUAL;
                case Constants.FILTER_OPERATOR_EQUAL:
                default:
                    return Constants.DB_OPERATOR_MATCH;
            }
        }
        return Constants.DB_OPERATOR_MATCH;
    }
}
