package org.acme.util;

import org.acme.model.QueryFields;

import java.util.ArrayList;
import java.util.List;

import lombok.experimental.UtilityClass;

@UtilityClass
public class QueryBuilder {

    public static String build(final List<QueryFields> fields) {
        StringBuilder sb = new StringBuilder("SELECT * FROM c WHERE ");
        List<String> queryList = new ArrayList<>();
        fields.forEach(filter -> {
            queryList.add(new StringBuilder().append("c.")
                    .append(filter.name)
                    .append("=")
                    .append("\"")
                    .append(filter.value)
                    .append("\"").toString());

        });

        sb.append(String.join(Constants.DB_OPERATOR_AND, queryList));

        return sb.toString();
    }
}
